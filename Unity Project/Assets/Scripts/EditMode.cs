﻿using UnityEngine;
using UnityEngine.UI;
using System.Xml;
using System.Collections.Generic;

public class EditMode : Singleton<EditMode>
{
    private GameObject editPanel;
    private GameObject EditPanel
    {
        get
        {
            if (editPanel == null)
                editPanel = ResourceManager.InstantiateEditPanel();
            return editPanel;
        }
    }
    private GameObject editCubeObj;
    private GameObject EditCubeObj
    {
        get
        {
            if (editCubeObj == null)
                editCubeObj = ResourceManager.InstantiateEditCube(new Vector3(0, 0.5f, 0));
            return editCubeObj;
        }
    }
    private EditCube editCube;
    private Material editCubeDeleteMat;
    private Material editcubeDefaultMat;
    private GameObject selectedCube;
    private Controller controller;

    private InputField levelName;
    private Text selectedName;
    private Text selectedId;

    private List<string> tagList = new List<string>();
    private string currentTag = "Wall";
    private float camSpeed = 50;
    private bool writing = false;
    private int doorKeyId = 0;

    void Start()
    {
        controller = new Controller();
        selectedCube = ResourceManager.GetWall();
        editCube = EditCubeObj.GetComponent<EditCube>();
        editcubeDefaultMat = editCube.GetComponent<Renderer>().material;
        editCubeDeleteMat = ResourceManager.GetEditCubeDeleteMaterial();
        selectedName = EditPanel.transform.FindChild("TilesPanel/SelectedName").GetComponent<Text>();
        selectedId = EditPanel.transform.FindChild("TilesPanel/SelectedId").GetComponent<Text>();
        levelName = EditPanel.transform.FindChild("LevelName").GetComponent<InputField>();

        levelName.text = "testLevel";
    }

    void OnEnable()
    {
        GameManager.Instance.editModeActive.Invoke(true);

        EditPanel.SetActive(true);
        EditCubeObj.SetActive(true);
    }

    void OnDisable()
    {
        GameManager.Instance.editModeActive.Invoke(false);
        EditPanel.SetActive(false);
        EditCubeObj.SetActive(false);
    }

    void MoveCam()
    {
        if (Input.GetAxis("Horizontal") != 0)
            Camera.main.transform.position += new Vector3(Input.GetAxis("Horizontal"), 0, 0) * Time.deltaTime * camSpeed;
        if (Input.GetAxis("Vertical") != 0)
            Camera.main.transform.position += new Vector3(0, 0, Input.GetAxis("Vertical")) * Time.deltaTime * camSpeed;
        if (Input.mouseScrollDelta != Vector2.zero)
            Camera.main.transform.position += Camera.main.transform.forward * Input.mouseScrollDelta.y * Time.deltaTime * camSpeed;
    }

    void ChangeCube()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            selectedCube = ResourceManager.GetPlayer();
        if (Input.GetKeyDown(KeyCode.Alpha2))
            selectedCube = ResourceManager.GetWall();
        if (Input.GetKeyDown(KeyCode.Alpha3))
            selectedCube = ResourceManager.GetCrate();
        if (Input.GetKeyDown(KeyCode.Alpha4))
            selectedCube = ResourceManager.GetSlot();
        if (Input.GetKeyDown(KeyCode.Alpha5))
            selectedCube = ResourceManager.GetDoor();
        if (Input.GetKeyDown(KeyCode.Alpha6))
            selectedCube = ResourceManager.GetKey();

        currentTag = selectedCube.name;
        if (selectedCube.GetComponent<Door>() != null)
            selectedCube.GetComponent<Door>().id = doorKeyId;
        else if (selectedCube.GetComponent<Key>() != null)
            selectedCube.GetComponent<Key>().id = doorKeyId;

        selectedName.text = currentTag;
        selectedId.text = doorKeyId.ToString();
    }

    void ChangeCube(string tileName)
    {
        currentTag = tileName;
        selectedCube = Resources.Load<GameObject>("Prefabs/" + tileName);

        selectedName.text = currentTag;
        selectedId.text = doorKeyId.ToString();
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Z))
            doorKeyId -= doorKeyId == 0 ? 0 : 1;
        if (Input.GetKeyDown(KeyCode.X))
            doorKeyId++;
        if (Input.GetKeyDown(KeyCode.F5))
            SaveLevel();
        if (Input.GetKeyDown(KeyCode.Return))
        {
            writing = !writing;
            if (writing)
                levelName.Select();
        }
        if (writing)
            return;

        if (Input.GetKeyDown(KeyCode.Q))
            EditCubeObj.transform.rotation = Quaternion.Euler(EditCubeObj.transform.rotation.eulerAngles + new Vector3(0, 90, 0));
        if (Input.GetKeyDown(KeyCode.E))
            EditCubeObj.transform.rotation = Quaternion.Euler(EditCubeObj.transform.rotation.eulerAngles + new Vector3(0, -90, 0));

        if (Camera.main.pixelRect.Contains(Input.mousePosition))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                Transform objectHit = hit.transform;
                EditCubeObj.transform.position = new Vector3(Mathf.Round(hit.point.x), 0.5f, Mathf.Round(hit.point.z));
            }

            if (Input.GetMouseButton(0) && !Input.GetKey(KeyCode.LeftAlt) && editCube.freePos)
            {
                GameObject go = Instantiate(selectedCube, EditCubeObj.transform.position, EditCubeObj.transform.rotation) as GameObject;
                go.name = currentTag;
            }
            else if (Input.GetMouseButton(0) && Input.GetKey(KeyCode.LeftAlt) && editCube.collidedObject != null)
            {
                Destroy(editCube.collidedObject);
                editCube.freePos = true;
            }
        }

        if (Input.GetKeyDown(KeyCode.LeftAlt))
            editCube.GetComponent<Renderer>().material = editCubeDeleteMat;
        if (Input.GetKeyUp(KeyCode.LeftAlt))
            editCube.GetComponent<Renderer>().material = editcubeDefaultMat;

        MoveCam();

        ChangeCube();
    }

    void SaveLevel()
    {
        SaveLevel(levelName.text);
    }

    void SaveLevel(string levelName)
    {
        XmlWriterSettings settings = new XmlWriterSettings();
        settings.Indent = true;
        using (XmlWriter writer = XmlWriter.Create(Application.dataPath + "/Resources/Levels/" + levelName + ".xml", settings))
        {
            writer.WriteStartDocument();
            writer.WriteStartElement("Root");

            foreach (GameObject g in GameObject.FindGameObjectsWithTag("Entity"))
            {
                writer.WriteStartElement("Tile");
                writer.WriteAttributeString("name", g.name);
                writer.WriteAttributeString("pos", g.transform.position.ToString());
                writer.WriteAttributeString("rot", g.transform.rotation.ToString());
                if (g.GetComponent<Related>() != null)
                    writer.WriteAttributeString("id", g.GetComponent<Related>().id.ToString());
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
            writer.WriteEndDocument();
        }

        Debug.Log("Level " + levelName + " is saved!");
    }
}
