﻿using UnityEngine;
using System;

public enum Panel
{
    Main,
    LevelPacks,
    Credits,
    LoadLevel,
    None
}

public class UIManager : Singleton<UIManager>
{
    public GameObject mainMenuPanel;
    public GameObject levelPacksPanel;
    public GameObject creditsPanel;
    public GameObject loadLevelPanel;

    public Action<bool> mainMenuActive;

    void Start()
    {
        SwitchToPanel(Panel.Main);
    }

    public void SwitchToPanel(Panel type)
    {
        if (mainMenuActive != null)
            mainMenuActive.Invoke(type == Panel.Main);

        loadLevelPanel.SetActive(type == Panel.LoadLevel);
        creditsPanel.SetActive(type == Panel.Credits);
        mainMenuPanel.SetActive(type == Panel.Main);
        levelPacksPanel.SetActive(type == Panel.LevelPacks);
    }
}
