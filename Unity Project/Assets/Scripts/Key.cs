﻿using UnityEngine;

public class Key : Related
{
    [HideInInspector]
    public bool on = false;
    private Door door;

    void Start()
    {
        Init();
    }

    void OnTriggerEnter()
    {
        on = true;
        door.CheckKeys();
    }

    void OnTriggerExit()
    {
        on = false;
        door.CheckKeys();
    }

    void Init()
    {
        foreach (Door d in FindObjectsOfType<Door>())
        {
            if (d.id == id)
            {
                door = d;
                break;
            }
        }
    }
}
