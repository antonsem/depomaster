﻿using UnityEngine;
using System.Collections;

public class Controller
{
    public float horizontal
    {
        get { return Input.GetAxisRaw("Horizontal"); }
    }

    public float vertical
    {
        get { return Input.GetAxisRaw("Vertical"); }
    }

    public float horizontalPressed
    {
        get
        {
            if (horizontal != 0 && !horizontalDown)
            {
                horizontalDown = true;
                return horizontal;
            }
            return 0;
        }
    }

    public float verticalPressed
    {
        get
        {
            if (vertical != 0 && !verticalDown)
            {
                verticalDown = true;
                return vertical;
            }
            return 0;
        }
    }

    public bool firePressed
    {
        get
        {
            bool retVal = false;
            Debug.Log(Input.GetAxis("Fire1") + " " + firedown);
            if (!firedown && Input.GetAxis("Fire1") != 0)
            {
                retVal = true;
                firedown = true;
            }
            return retVal;
        }
    }

    private bool horizontalDown = false;
    private bool verticalDown = false;
    private bool firedown = false;

    public void Tick()
    {
        if (horizontal == 0 && horizontalDown)
            horizontalDown = false;
        if (vertical == 0 && verticalDown)
            verticalDown = false;
        if (Input.GetAxisRaw("Fire1") == 0 && firedown)
            firedown = false;
    }
}
