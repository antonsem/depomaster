﻿using UnityEngine;

public class EditCube : MonoBehaviour
{
    public bool freePos = true;
    public GameObject collidedObject;

    void OnTriggerEnter(Collider col)
    {
        freePos = false;
        collidedObject = col.gameObject;
    }

    void OnTriggerExit()
    {
        freePos = true;
        collidedObject = null;
    }
}
