﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Xml;
using System.Collections.Generic;

public class GameManager : Singleton<GameManager>
{
    public Text levelPackList;
    [HideInInspector]
    public List<Slot> slots = new List<Slot>();
    private List<Level> levels = new List<Level>();
    private EditMode editMode;
    private int levelIndex = 0;
    private XmlNode rootNode;
    [HideInInspector]
    public bool canEdit = false;
    [HideInInspector]
    public bool levelLoaded = false;

    public Action<bool> editModeActive;

    void Start()
    {
        XmlDocument xDoc = new XmlDocument();
        xDoc.LoadXml(Resources.Load<TextAsset>("Levels/LevelPacks").text);
        rootNode = xDoc.SelectSingleNode("Root");

        if (levelPackList == null)
        {
            Debug.LogError("levelPackList is not assigned!");
            GetPack();
        }
        else
        {
            levelPackList.text = "";
            for (int i = 0; i < rootNode.ChildNodes.Count; i++)
                levelPackList.text += (i + 1).ToString() + " : " + rootNode.ChildNodes[i].Attributes["name"].Value + "\n";
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            DestroyLevel();
            InitLevel(levels[levelIndex]);
        }
        if (Input.GetKeyDown(KeyCode.F2) && canEdit)
        {
            if (editMode == null)
                editMode = EditMode.Instance;
            else
                editMode.enabled = !editMode.enabled;
        }
        if (Input.GetKeyDown(KeyCode.F3) && canEdit)
            UIManager.Instance.SwitchToPanel(Panel.LoadLevel);
        if (Input.GetKeyDown(KeyCode.F9))
            NextLevel();
        if (Input.GetKeyDown(KeyCode.Alpha0))
            UIManager.Instance.SwitchToPanel(Panel.Main);

    }

    public bool GetPack(int index)
    {
        if (rootNode.ChildNodes.Count > index)
        {
            levelIndex = 0;
            GetPack(rootNode.ChildNodes.Item(index).Attributes["name"].Value);
            return true;
        }
        return false;
    }

    void GetPack(string packName = "")
    {
        if (string.IsNullOrEmpty(packName))
            ReadLevels(rootNode.SelectSingleNode("Path"));
        else
        {
            foreach (XmlNode childNode in rootNode.SelectNodes("Pack"))
            {
                if (childNode.Attributes["name"].Value == packName)
                    ReadLevels(childNode);
            }
        }
    }

    void ReadLevels(XmlNode node = null)
    {
        levels.Clear();
        if (node != null)
        {
            foreach (XmlNode childNode in node.ChildNodes)
                levels.Add(LoadLevel(childNode.Attributes["name"].Value));
        }
        else
            levels.Add(LoadLevel("testLevel"));

        InitLevel(levels[levelIndex]);
    }

    public void LoadSingleLevel(string xml)
    {
        XmlDocument xDoc = new XmlDocument();
        xDoc.LoadXml(xml);
        XmlNode root = xDoc.SelectSingleNode("Root");
        levels.Clear();
        levels.Add(new Level(root));
        levelIndex = 0;
        DestroyLevel();
        InitLevel(levels[levelIndex]);
    }

    void InitLevel(Level newLevel)
    {
        DestroyLevel();
        foreach (KeyValuePair<TileData, string> pair in newLevel.posPrefabDict)
        {
            try
            {
                GameObject go = Instantiate(Resources.Load<GameObject>("Prefabs/" + pair.Value), pair.Key.pos, pair.Key.rot) as GameObject;
                go.name = pair.Value;
                if (go.GetComponent<Related>() != null)
                    go.GetComponent<Related>().id = pair.Key.id;
            }
            catch
            {
                Debug.LogError("Cannot find prefab: " + pair.Value);
            }
        }
        levelLoaded = true;
    }

    void DestroyLevel()
    {
        foreach (GameObject g in GameObject.FindGameObjectsWithTag("Entity"))
            Destroy(g);

        slots.Clear();
    }

    public void NextLevel()
    {
        if (levelIndex < levels.Count - 1)
            levelIndex++;
        else
            Debug.LogError("GameOver!");

        DestroyLevel();
        InitLevel(levels[levelIndex]);
    }

    public bool CheckIfComplete()
    {
        foreach (Slot s in slots)
        {
            if (s == null)
            {
                slots.Remove(s);
                continue;
            }
            if (!s.occupied)
                return false;
        }

        NextLevel();
        return true;
    }

    Level LoadLevel(string levelName)
    {
        XmlDocument xDoc = new XmlDocument();
        xDoc.LoadXml(Resources.Load<TextAsset>("Levels/" + levelName).text);
        XmlNode root = xDoc.SelectSingleNode("Root");
        if (root != null)
        {
            Level newLevel = new Level(root);
            return newLevel;
        }
        Debug.LogError("Root element was not found in " + levelName + "!");
        return null;
    }

    public static Vector3 Vector3Parse(string s)
    {
        s = s.Trim(new char[] { '(', ')' });
        Vector3 vector = Vector3.zero;
        vector.x = float.Parse(s.Split(',')[0]);
        vector.y = float.Parse(s.Split(',')[1]);
        vector.z = float.Parse(s.Split(',')[2]);
        return vector;
    }

    public static Quaternion QuaternionParse(string s)
    {
        s = s.Trim(new char[] { '(', ')' });
        Quaternion rot = new Quaternion();
        rot.x = float.Parse(s.Split(',')[0]);
        rot.y = float.Parse(s.Split(',')[1]);
        rot.z = float.Parse(s.Split(',')[2]);
        rot.w = float.Parse(s.Split(',')[3]);
        return rot;
    }

    public static TileData GetPosRot(XmlNode node)
    {
        TileData data = new TileData();
        data.pos = Vector3Parse(node.Attributes["pos"].Value);

        if (node.Attributes["rot"] != null)
            data.rot = QuaternionParse(node.Attributes["rot"].Value);
        else
            data.rot = Quaternion.identity;

        if (node.Attributes["id"] != null)
            data.id = int.Parse(node.Attributes["id"].Value);

        return data;

    }
}
