﻿using UnityEngine;
using UnityEngine.UI;

public class LoadLevelPanel : MonoBehaviour
{
    public InputField input;

    void OnEnable()
    {
        GetLevel();
    }

    void GetLevel()
    {
        input.text = "<Root>\n";
        foreach (GameObject g in GameObject.FindGameObjectsWithTag("Entity"))
        {
            input.text += "<Tile name=\"" + g.name + "\" pos=\"" +
                g.transform.position.ToString() + "\"" +
                " rot=\"" + g.transform.rotation.ToString() + "\"";
            if (g.GetComponent<Related>() != null)
                input.text += " id=\"" + g.GetComponent<Related>().id.ToString() + "\"";
            input.text += "/>\n";
        }
        input.text += "</Root>";
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F3))
            UIManager.Instance.SwitchToPanel(Panel.None);
        if (Input.GetKeyDown(KeyCode.F4))
            GameManager.Instance.LoadSingleLevel(input.text);
    }
}
