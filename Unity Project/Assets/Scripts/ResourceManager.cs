﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public static class ResourceManager
{
    public static void InstantiateWall(Vector3 pos)
    {
        GameObject.Instantiate(GetWall(), pos, Quaternion.identity);
    }

    public static GameObject GetWall()
    {
        return Resources.Load("Prefabs/Wall") as GameObject;
    }

    public static void InstantiateSlot(Vector3 pos)
    {
        GameObject.Instantiate(GetSlot(), pos, Quaternion.identity);
    }

    public static GameObject GetSlot()
    {
        return Resources.Load("Prefabs/Slot") as GameObject;
    }

    public static void InstantiateCrate(Vector3 pos)
    {
        GameObject.Instantiate(GetCrate(), pos, Quaternion.identity);
    }

    public static GameObject GetCrate()
    {
        return Resources.Load("Prefabs/Crate") as GameObject;
    }

    public static void InstantiatePlayer(Vector3 pos)
    {
        GameObject.Instantiate(GetPlayer(), pos, Quaternion.identity);
    }

    public static GameObject GetPlayer()
    {
        return Resources.Load("Prefabs/Player") as GameObject;
    }

    public static void InstantiateDoor(Vector3 pos)
    {
        GameObject.Instantiate(GetDoor(), pos, Quaternion.identity);
    }

    public static GameObject GetDoor()
    {
        return Resources.Load("Prefabs/Door") as GameObject;
    }

    public static void InstantiateKey(Vector3 pos)
    {
        GameObject.Instantiate(GetKey(), pos, Quaternion.identity);
    }

    public static GameObject GetKey()
    {
        return Resources.Load("Prefabs/Key") as GameObject;
    }

    public static GameObject InstantiateEditPanel()
    {
        GameObject go = GameObject.Instantiate(Resources.Load("Prefabs/UI/EditPanel"), Vector3.zero, Quaternion.identity) as GameObject;
        go.transform.SetParent(GameObject.FindWithTag("Canvas").transform, false);
        return go;
    }

    public static GameObject InstantiateEditCube(Vector3 pos)
    {
        return GameObject.Instantiate(GetEditCube(), pos, Quaternion.identity) as GameObject;
    }

    public static GameObject GetEditCube()
    {
        return Resources.Load("Prefabs/EditCube") as GameObject;
    }

    public static Material GetEditCubeDeleteMaterial()
    {
        return Resources.Load<Material>("Materials/EditCubeDelete");
    }

    public static Button GetTileButton()
    {
        GameObject go = GameObject.Instantiate(Resources.Load("Prefabs/UI/TileButton"), Vector3.zero, Quaternion.identity) as GameObject;
        go.transform.SetParent(GameObject.FindWithTag("TileContent").transform, false);
        return go.GetComponent<Button>();
    }
}
