﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MusicManager : MonoBehaviour
{
    private AudioSource source;
    private int trackNo = 0;
    public AudioClip[] playList;

    void Start()
    {
        source = GetComponent<AudioSource>();
        if (playList == null || playList.Length == 0)
        {
            Debug.LogError("Playlist is empty");
            enabled = false;
        }
    }

    void Update()
    {
        if (!source.isPlaying)
        {
            if (trackNo >= playList.Length)
                trackNo = 0;

            source.PlayOneShot(playList[trackNo]);
            trackNo++;
        }
        if (Input.GetKeyDown(KeyCode.M))
            source.volume = source.volume == 0 ? 1 : 0;
    }
}
