﻿using UnityEngine;
using System.Collections;

public class Player : Movable
{
    private Controller controller;
    private Transform camPos;
    private GameManager manager;
    private bool canMove = true;
    private bool rotating = false;

    void Start()
    {
        controller = new Controller();
        manager = GameManager.Instance;
        manager.editModeActive += SetCanMoveState;
        UIManager.Instance.mainMenuActive += SetCanMoveState;
        camPos = transform.FindChild("CameraPos");
        foreach (Player p in FindObjectsOfType<Player>())
        {
            if (p != this)
                Destroy(p.gameObject);
        }
    }

    void OnDestroy()
    {
        manager.editModeActive -= SetCanMoveState;
        UIManager.Instance.mainMenuActive -= SetCanMoveState;
    }

    void SetCanMoveState(bool state)
    {
        canMove = !state;
    }

    IEnumerator Turn(Vector3 dir)
    {
        rotating = true;
        Quaternion newRotation = Quaternion.LookRotation(dir);
        while (Quaternion.Angle(transform.rotation, newRotation) > 5)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * 15);
            yield return null;
        }
        transform.rotation = newRotation;
        rotating = false;
    }

    void Update()
    {
        controller.Tick();
        if (canMove && controller.horizontal != 0)
        {
            if (!rotating)
                StartCoroutine(Turn(new Vector3(controller.horizontal, 0, 0).normalized));
            CanMove(new Vector3(controller.horizontal, 0, 0).normalized, true);
            manager.CheckIfComplete();
        }
        if (canMove && controller.vertical != 0)
        {
            if (!rotating)
                StartCoroutine(Turn(new Vector3(0, 0, controller.vertical).normalized));
            CanMove(new Vector3(0, 0, controller.vertical).normalized, true);
            manager.CheckIfComplete();
        }
        if (canMove)
            Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, transform.position + new Vector3(0, 6, -7), 0.1f);
    }
}
