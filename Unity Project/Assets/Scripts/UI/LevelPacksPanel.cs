﻿using UnityEngine;

public class LevelPacksPanel : MonoBehaviour
{

    UIManager uiManager;
    GameManager manager;

    void Start()
    {
        uiManager = UIManager.Instance;
        manager = GameManager.Instance;
    }

    void Update()
    {
        for (int i = 0; i < 9; i++)
        {
            if (Input.GetKeyDown((KeyCode)(49 + i)) && manager.GetPack(i))
            {
                uiManager.SwitchToPanel(Panel.None);
                manager.canEdit = false;
            }
        }
        if (Input.GetKeyDown(KeyCode.Alpha0))
            uiManager.SwitchToPanel(Panel.Main);
    }
}
