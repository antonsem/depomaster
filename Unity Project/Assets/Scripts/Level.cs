﻿using UnityEngine;
using System.Xml;
using System.Collections.Generic;

public struct TileData
{
    public Vector3 pos;
    public Quaternion rot;
    public int id;
}

public class Level
{
    public Dictionary<TileData, string> posPrefabDict;

    public Level(XmlNode rootNode)
    {
        posPrefabDict = new Dictionary<TileData, string>();

        foreach (XmlNode node in rootNode.ChildNodes)
        {
            if (node.Name == "Tile")
            {
                try
                {
                    posPrefabDict.Add(GameManager.GetPosRot(node), node.Attributes["name"].Value);
                }
                catch
                {
                    Debug.Log("Dublicate pos: " + node.Name + " - " + node.Attributes["pos"].Value);
                }
            }
        }
    }
}
