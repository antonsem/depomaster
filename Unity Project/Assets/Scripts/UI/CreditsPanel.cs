﻿using UnityEngine;

public class CreditsPanel : MonoBehaviour
{
    UIManager uiManager;

    void Start()
    {
        uiManager = UIManager.Instance;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha0))
            uiManager.SwitchToPanel(Panel.Main);
    }
}
