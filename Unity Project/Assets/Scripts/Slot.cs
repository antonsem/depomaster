﻿using UnityEngine;

public class Slot : MonoBehaviour
{
    public bool occupied = false;

    void OnTriggerEnter()
    {
        occupied = true;
    }

    void OnTriggerExit()
    {
        occupied = false;
    }

    void Start()
    {
        GameManager.Instance.slots.Add(this);
    }
}
