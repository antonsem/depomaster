﻿using UnityEngine;

public class MainMenuPanel : MonoBehaviour
{
    private UIManager uiManager;
    private GameManager manager;
    public GameObject continueText;

    void Start()
    {
        manager = GameManager.Instance;
        uiManager = UIManager.Instance;
    }

    void OnEnable()
    {
        if (manager != null)
            continueText.SetActive(manager.levelLoaded);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            uiManager.SwitchToPanel(Panel.LevelPacks);
        else if (Input.GetKeyDown(KeyCode.Alpha2) && manager.GetPack(0))
        {
            uiManager.SwitchToPanel(Panel.None);
            manager.canEdit = true;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
            uiManager.SwitchToPanel(Panel.Credits);
        else if (Input.GetKeyDown(KeyCode.Alpha4) && manager.levelLoaded)
            uiManager.SwitchToPanel(Panel.None);
    }
}
