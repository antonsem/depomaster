﻿using UnityEngine;
using System.Collections;

public class Movable : MonoBehaviour
{
    private bool isMoving = false;
    private Vector3 startPosition;
    private Vector3 endPosition;
    private float movePercent;
    private Vector3 input;
    private float moveSpeed = 5f;

    IEnumerator Move(Transform transform)
    {
        isMoving = true;
        startPosition = transform.position;
        movePercent = 0;

        endPosition = new Vector3(startPosition.x + System.Math.Sign(input.x),
            startPosition.y, startPosition.z + System.Math.Sign(input.z));

        while (movePercent < 1f)
        {
            movePercent += Time.deltaTime * moveSpeed;
            transform.position = Vector3.Lerp(startPosition, endPosition, movePercent);
            yield return null;
        }

        isMoving = false;
    }

    public bool CanMove(Vector3 direction, bool pushCrate = false)
    {
        if (isMoving)
            return false;

        RaycastHit hit;
        Ray ray = new Ray(transform.position, direction);
        Physics.Raycast(ray, out hit, 1);

        input = direction;

        if ((hit.collider == null || hit.collider.isTrigger)
            || (hit.collider.GetComponent<Movable>() != null
            && pushCrate && hit.collider.GetComponent<Movable>().CanMove(direction)))
        {
            StartCoroutine(Move(transform));
            return true;
        }
        return false;
    }
}
