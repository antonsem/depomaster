﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Door : Related
{
    private float openSpeed = 5;
    private Transform pivot;
    private Collider col;
    private Vector3 openRotation = new Vector3(0, 90, 0);
    private Vector3 desiredRotation = Vector3.zero;
    private List<Key> keys = new List<Key>();
    private bool canClose = true;

    void Start()
    {
        pivot = transform.FindChild("Pivot");
        foreach (Key k in FindObjectsOfType<Key>())
        {
            if (k.id == id)
                keys.Add(k);
        }
        col = GetComponent<Collider>();
    }

    public void CheckKeys()
    {
        foreach (Key k in keys)
        {
            if (!k.on)
            {
                desiredRotation = Vector3.zero;
                return;
            }
        }
        desiredRotation = openRotation;
    }

    void OnTriggerStay(Collider col)
    {
        canClose = false;
    }

    void OnTriggerExit()
    {
        canClose = true;
    }

    void Update()
    {
        if (pivot.transform.localRotation.eulerAngles != desiredRotation && canClose)
            pivot.transform.localRotation = Quaternion.Euler(Vector3.Lerp(pivot.transform.localRotation.eulerAngles, desiredRotation, openSpeed * Time.deltaTime));
    }
}
